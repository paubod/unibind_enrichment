#!/bin/sh

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Initialise directories for the installed libraries.

LOCALDIR="$BASEDIR/lib/usr/local"

# Set labels for SELinux, apply the labelling for SELinux.

semanage fcontext -a -t bin_t "$LOCALDIR/bin(/.*)?"
semanage fcontext -a -t lib_t "$LOCALDIR/lib(/.*)?"
semanage fcontext -a -t lib_t "$LOCALDIR/lib64(/.*)?"
restorecon -R "$LOCALDIR"
