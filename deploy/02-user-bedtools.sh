#!/bin/sh

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain bedtools.

WORKDIR="$THISDIR/work"
CLONEDIR="$WORKDIR/bedtools2"

mkdir "$WORKDIR"
git clone https://github.com/arq5x/bedtools2.git "$CLONEDIR"

# Patch bedtools.

patch -N -d "$CLONEDIR" -p0 < "$THISDIR/patches/patch-bedtools-scripts.diff"

# Build and install bedtools.

cd "$CLONEDIR"
make
make install DESTDIR="$BASEDIR/lib"
