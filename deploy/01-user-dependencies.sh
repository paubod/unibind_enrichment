#!/bin/sh

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Initialise local dependencies.

python3 -m pip install --root "$BASEDIR/lib" -r "$BASEDIR/requirements.txt"

mkdir -p "$BASEDIR/lib/usr/local/lib/R/library"
R_LIBS="$BASEDIR/lib/usr/local/lib/R/library" Rscript "$BASEDIR/requirements.R"
