UniBind TFBS sets enrichment toolkit
====================================

The UniBind enrichment tool predicts which sets of TFBSs from the UniBind_
database are enriched in a set of given genomic regions. Enrichment
computations are performed using the LOLA_ tool. For more information about
the underlying enrichment computations, read the `LOLA documentation`_. The
underlying database of TFBS sets from UniBind in provided in the RDS file
``data/20190423_UniBind_LOLA.RDS`` (provided here), which has been precomputed
using LOLA. The tool allows for three types of computations:

- Enrichment of TFBSs in a set of genomic regions compared to a given
  universe of genomic regions.
- Differential TFBS enrichment when comparing one set of genomic regions
  (*S1*) to another (*S2*).
- Enrichment of TFBSs in a set of genomic regions compared to all TFBS sets
  stored in UniBind.

.. _LOLA: http://code.databio.org/LOLA/
.. _`LOLA documentation`: http://code.databio.org/LOLA/
.. _UniBind: https://unibind.uio.no/


.. contents::


Getting started
---------------

To be able to use this software, you need to download the project to your
machine and follow the steps below to install and set up the necessary software
dependencies.

Prerequisites
+++++++++++++

The following tools are needed to run this software:

============= =============== ================================================
Tool          Package         Details
============= =============== ================================================
``bash``      ``bash``        `GNU Bash`_ - command processing
``bedtools``                  BEDtools_ (version 2) - genome arithmetic tools
Python        ``python3``     Requires packages to prepare swarm plots
R             ``R``           Requires various processing and output packages
============= =============== ================================================

Suitable packages should be available via your system's software package
manager. The package names given in this document apply to the Fedora
GNU/Linux distribution. Similar packages may be available for other
distributions.

To automate system package installation using Fedora, review the
``requirements-sys.txt`` file containing package names, then pass these names
to a suitable install tool running with the necessary privileges. For example:

.. code:: shell

  xargs dnf install < requirements-sys.txt

(This presents the list of packages to the ``dnf install`` tool.)

.. _BEDtools: http://bedtools.readthedocs.io/en/latest/content/installation.html
.. _`GNU Bash`: https://www.gnu.org/software/bash

Prerequisite: Python
++++++++++++++++++++

Note that Python should be pre-installed on your Linux distribution. Check
this by typing...

.. code:: shell

   whereis python

If it is not available, you can install it from python.org_. After
installation, there are several required libraries that have to be installed.

This software has previously employed Python 2.7 but should also run with
Python 3, with Python 3.7 having been tested.

.. _python.org: https://www.python.org/downloads/

Library dependencies
''''''''''''''''''''

To be able to prepare swarm plots, a number of Python packages need to be
installed for the Python environment.

These can be installed using the ``pip`` tool which may already be available
alongside other Python programs on your system. If not, you can install it via
your software package manager following the `pip tutorial`_.

With ``pip``, the packages can be installed in the shell command environment
as follows:

.. code:: shell

  pip install -r requirements.txt

Or without the actual ``pip`` command using ``python``:

.. code:: shell

  python -m pip install -r requirements.txt

.. _`pip tutorial`: https://pip.pypa.io/en/stable/installing/

Prerequisite: R
+++++++++++++++

You can install the latest version of the R statistical environment from CRAN_
or by using the software package manager on your system. The minimum required
R version is 2.5.0.

If installing from source, the following system-level packages may need to be
installed:

::

  gcc-gfortran java-1.8.0-openjdk-devel libcurl-devel libX11-devel libXt-devel
  pcre-devel readline-devel

.. _CRAN: https://cran.r-project.org/

Library dependencies
''''''''''''''''''''

Various libraries and tools are required within the R environment to run the
software, and these may need to be built against lower-level libraries. The
following system-level packages may therefore need to be installed:

::

  cairo-devel libcurl-devel libxml2-devel openssl-devel pandoc xz-devel
  zlib-devel

(These package names being suitable for Red Hat or Fedora distributions.)

The following command can be run to install required software packages:

.. code:: shell

  Rscript requirements.R

Using UniBind enrichment tools
------------------------------

A summary of the different operating modes of the tools is given below.

Enrichment within a given universe of genomic regions
+++++++++++++++++++++++++++++++++++++++++++++++++++++

.. image:: img/oneSetBg.png
   :alt: Enrichment with a background

To compute which sets of TFBSs from UniBind are enriched in a set *S* of
genomic regions compared to a universe *U* of genomic regions, you can use the
``oneSetBg`` subcommand as follows.

.. code:: shell

   bash bin/UniBind_enrich.sh oneSetBg <LOLA db> <S bed> <U bed> <output dir>

This will compute the enrichment of TFBS sets from UniBind (using
``data/20190423_UniBind_LOLA.RDS``) in the genomic regions from *S* (provided
as a BED file) when compared to the expectation from a universe *U* of genomic
regions (provided as a BED file). All result files will be provided in the
``<output dir>`` directory.

.. admonition:: Note

  Every region in *S* should overlap with one region in *U*.

Differential enrichment
+++++++++++++++++++++++

.. image:: img/oneTwoSets.png
   :alt: Differential enrichment

To compute which sets of TFBSs from UniBind are enriched in a set *S1*
of genomic regions compared to another set *S2* of genomic regions, you
can use the ``twoSets`` subcommand as follows.

.. code:: shell

   bash bin/UniBind_enrich.sh twoSets <LOLA db> <S1 bed> <S2 bed> <output dir>

This will compute the enrichment of TFBS sets from UniBind (using
``data/20190423_UniBind_LOLA.RDS``) in the genomic regions from *S1* (provided
as a BED file) when compared to the genomic regions in *S2* (provided as a BED
file). All result files will be provided in the ``<output dir>`` directory.

Enrichment when no background is provided
+++++++++++++++++++++++++++++++++++++++++

.. image:: img/oneSetNoBg.png
   :alt: Enrichment with no background

When no background is provided, one can compute which sets of TFBSs from
UniBind are enriched in a set *S* of genomic regions using the ``oneSetNoBg``
subcommand as follows. In this case, the enrichment will be computed against a
default background corresponding to the genomic regions of all TFBSs stored in
UniBind.

.. admonition:: Warning

  We encourage users to provide an adequate background set of genomic regions
  whenever possible using the ``oneSetBg`` subcommand.

.. code:: shell

   bash bin/UniBind_enrich.sh oneSetNoBg <LOLA db> <LOLA universe> <S bed> <output dir>

This will compute the enrichment of TFBS sets from UniBind (using
``data/20190423_UniBind_LOLA.RDS`` and
``data/20190423_UniBind_LOLAuniverse.RDS``) in the genomic regions from *S*
(provided as a BED file). All result files will be provided in the ``<output
dir>`` directory.

Output
++++++

The output directory will contain the ``allEnrichments.tsv`` file that provides
the enrichment score for each TFBS set from UniBind along with their metadata
information. Similar files (following the template ``col_.tsv``) are created
for each TF with all data sets available for that TF.

A visual representation of the enrichment analysis is provided in the output
directory with three plots:

1. A swarm plot using the log10(p-value) of the enrichment for each TFBS
   set on the y-axis.

   File: ``allEnrichments_swarm.pdf``

2. An interactive `beeswarm plot`_ showing the 10 most enriched TFs.

   File: ``Unibind_enrichment_interactive_beeswarmplot.html``

3. An interactive `ranking plot`_ showing the log10(p-value) for all the
   datasets in UniBind.

   File: ``Unibind_enrichment_interactive_ranking.html``

In the three plots, the data sets for the top 10 TFs showing a log10(p-value)
< 3 are highlighted with dedicated colors (one color per TF). Data sets with
log10(p-value) > 3 are provided with a color for N.S.  (non-significant).

Users can explore the results with the **interactive plots**. These plots
allow to hide/show the top 10 enriched TFs, and the tooltip displays the TF
name, log10(p-value) (also known as significance), and cell type and
treatment.

.. _`beeswarm plot`: https://chart-studio.plot.ly/~jaimicore/23
.. _`ranking plot`: https://chart-studio.plot.ly/~jaimicore/25/#/

Example
+++++++

As an example of application, we provide data derived from the
publication `DNA methylation at enhancers identifies distinct breast
cancer lineages, Fleischer, Tekpli, et al, Nature Communications,
2017 <https://www.nature.com/articles/s41467-017-00510-x>`__. The
genomic regions of interest correspond to 200bp-long regions around CpGs
from cluster 2A described in the publication. These regions around CpGs
of interest are shown to be associated with FOXA1, GATA, and ESR1
binding. We applied the following command to compute TFBS enrichment
using all the CpG probes from the Illumina Infinium HumanMethylation450
microarray:

.. code:: shell

   bash bin/UniBind_enrich.sh oneSetBg data/20190423_UniBind_LOLAdb.RDS \
                              data/example_Fleischer_et_al/clusterA_200bp_hg38.bed \
                              data/example_Fleischer_et_al/450k_probes_hg38_200bp.bed \
                              ub_enrichment

We observe a clear enrichment for TFBSs associated with the expected
TFs. The corresponding swarm plot is:

.. image:: data/example_Fleischer_et_al/allEnrichments_swarm.png
   :alt: Swarm plot

A complementary beeswarm plot allows to clearly visualize the 10 most
enriched TFs.

.. image:: data/example_Fleischer_et_al/Unibind_enrichment_beeswarmplot.jpeg
   :alt: Swarm plot showing the ten most enriched TFs

The enrichment and ranking of all the datasets can be visualized in the
ranking plot.

.. image:: data/example_Fleischer_et_al/Unibind_enrichment_ranking.jpeg
   :alt: Enrichment and ranking of all datasets
