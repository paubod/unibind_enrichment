#!/bin/sh

# Structure derived from https://gist.github.com/waylan/4080362

ProgName=$(basename $0);
# /!\/!\/!\/!\
# This bash script should be located in the same directory as lola.R and
# lola_two_sets.R
DirName=$(dirname $0);

# Location of required libraries.

Lib="$DirName/../lib"

if [ -e "$Lib" ] ; then
    Prefix="$Lib/usr/local"
    PATH="$Prefix/bin:$PATH"
    PyVersion=`python3 -c 'import sys; print("%s.%s" % (sys.version_info.major, sys.version_info.minor))'`
    Python="python${PyVersion:-3.7}"
    export PYTHONPATH="$Prefix/lib/$Python/site-packages:$Prefix/lib64/$Python/site-packages"
    export R_LIBS="$Prefix/lib/R/library"
else
    Python="python"
fi

sub_help(){
    echo "Usage: $ProgName <subcommand> [options]\n";
    echo "Subcommands:";
    echo "    oneSetBg   Apply one set enrichment analysis against a background";
    echo "    usage: $ProgName oneSetBg <loladb> <bed file> <universe/background bed> <output dir>";
    echo "    oneSetNoBg   Apply one set enrichment analysis";
    echo "    usage: $ProgName oneSetNoBg <loladb> <lola universe> <bed file> <output dir>";
    echo "    twoSets  Apply two sets differential enrichment analysis";
    echo "    usage: $ProgName twoSets <loladb> <1st bed file> <2nd bed file> <output dir>";
    echo "";
    echo "For help with each subcommand run:";
    echo "$ProgName <subcommand> -h|--help";
    echo "";
}
  
sub_oneSetBg(){
    echo "Running 'oneSetBg' analysis";
    tmpin=$(mktemp);
    bedtools sort -i $2 | bedtools merge -i stdin > $tmpin;
    tmpbg=$(mktemp);
    bedtools sort -i $3 | bedtools merge -i stdin > $tmpbg;
    
    R --silent --slave --vanilla -f $DirName"/lola.R" \
        --args $1 $tmpin $tmpbg $4 $DirName"/LOLA_modif";
    rm $tmpin $tmpbg;
    
    $Python $DirName"/swarm_plot.py" \
	   -f $4/allEnrichments.tsv \
	   -o $4/allEnrichments_swarm.pdf ;
    
    Rscript $DirName"/UniBind_enrichment_swarmplot.R" \
	    -t $4/allEnrichments.tsv \
	    -o $4 ;
}
  
sub_oneSetNoBg(){
    echo "Running 'oneSetNoBg' analysis";
    tmpin=$(mktemp);
    bedtools sort -i $3 | bedtools merge -i stdin > $tmpin;
    R --silent --slave --vanilla -f $DirName"/lola_no_background.R" \
        --args $1 $2 $tmpin $4 $DirName"/LOLA_modif";
    rm $tmpin;
    
    $Python $DirName"/swarm_plot.py" -f $4/allEnrichments.tsv \
	   -o $4/allEnrichments_swarm.pdf ;
    
    Rscript $DirName"/UniBind_enrichment_swarmplot.R" \
	    -t $4/allEnrichments.tsv \
	    -o $4 ;
}

 sub_twoSets(){
    echo "Running 'twoSets' analysis";
    mkdir -p $4;
    tmpone=$(mktemp);
    tmptwo=$(mktemp);
    bedtools subtract -a $2 -b $3 | bedtools sort -i stdin | \
        bedtools merge -i stdin > $tmpone;
    bedtools subtract -b $2 -a $3 | bedtools sort -i stdin | \
        bedtools merge -i stdin > $tmptwo;
    nameone=$(basename $2);
    nametwo=$(basename $3);
    
    R --silent --slave --vanilla -f $DirName"/lola_two_sets.R" \
      --args $1 $tmpone $tmptwo $4 $DirName"/LOLA_modif";
    
    $Python $DirName"/swarm_plot.py" \
	   -f $4/allEnrichments.tsv  \
	   -o $4/allEnrichments_swarm.pdf ;
    
    Rscript $DirName"/UniBind_enrichment_swarmplot.R" \
	    -t $4/allEnrichments.tsv \
	    -o $4 ;
}
  
subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift;
        sub_${subcommand} $@;
        if [ $? = 127 ]
        then
            echo "Error: '$subcommand' is not a known subcommand." >&2;
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2;
            exit 1;
        fi
        ;;
esac
